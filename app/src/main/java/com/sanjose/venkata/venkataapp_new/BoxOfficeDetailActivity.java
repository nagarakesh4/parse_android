package com.sanjose.venkata.venkataapp_new;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;


public class BoxOfficeDetailActivity extends Activity {
    private ImageView ivPosterImage;
    private TextView tvTitle;
    private TextView tvSynopsis;
    private TextView tvCast;
    private TextView tvAudienceScore;
    private TextView tvCriticsScore;
    //private TextView tvCriticsConsensus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_office_detail);
        // Fetch views
        ivPosterImage = (ImageView) findViewById(R.id.ivPosterImage);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvSynopsis = (TextView) findViewById(R.id.tvSynopsis);
        tvCast = (TextView) findViewById(R.id.tvCast);
        //tvCriticsConsensus = (TextView) findViewById(R.id.tvCriticsConsensus);
        tvAudienceScore =  (TextView) findViewById(R.id.tvAudienceScore);
        tvCriticsScore = (TextView) findViewById(R.id.tvCriticsScore);
        // Use the movie to populate the data into our views
        BoxOfficeMovie movie = (BoxOfficeMovie)
                getIntent().getSerializableExtra(BoxOfficeActivity.MOVIE_DETAIL_KEY);
        final ProgressDialog progressDialog = ProgressDialog.show(
                BoxOfficeDetailActivity.this, "",
                "Loading info of "+movie.getTitle()+"....", true);
        setTitle(movie.getTitle());

        loadMovie(movie);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        }, 3000);
    }

    // Populate the data for the movie
    public void loadMovie(BoxOfficeMovie movie) {
        // Populate data
        tvTitle.setText(movie.getTitle());
        tvCriticsScore.setText(Html.fromHtml("<b>Critics Score:</b> " + movie.getCriticsScore() + "%"));
        tvAudienceScore.setText(Html.fromHtml("<b>Audience Score:</b> " + movie.getAudienceScore() + "%"));
        tvCast.setText(movie.getCastList());
        tvSynopsis.setText(Html.fromHtml("<b>Synopsis:</b> " + movie.getSynopsis()));
        //tvCriticsConsensus.setText(Html.fromHtml("<b>Consensus:</b> " + movie.getCriticsConsensus()));
        // R.drawable.large_movie_poster from
        // http://content8.flixster.com/movie/11/15/86/11158674_pro.jpg -->
        String[] posterUrl = movie.getPosterUrl().split("/");
        Log.i("posterUrl", String.valueOf(posterUrl.length));
        URI uri = null;
        try {
            uri = new URI(movie.getPosterUrl());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        String path = uri.getPath();
        String imagePath = "";
        int countIndex = 0;
        int index = path.indexOf("/");
        while (index >= 0) {
            Log.i("indexingggg", String.valueOf(index));
            Log.i("substringing", path.substring(index, path.length()));
            index = path.indexOf("/", index + 1);
            countIndex++;
            if(countIndex == 3){
                Log.i("rotentingnnggg", path.substring(index, path.length()));
                imagePath = path.substring(index, path.length());
            }
        }
        String imageNewURL = "http://content6.flixster.com" + imagePath;
        Log.i("is this what??", imageNewURL);
        Picasso.with(this).load(imageNewURL).
                placeholder(R.drawable.ic_launcher_large_poster).
                        into(ivPosterImage);
    }

}
