package com.sanjose.venkata.venkataapp_new;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;



import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {

    private ListView mListView;

    private List<Concert> mConcertList;

    int moviesCount = 0;

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_news, container, false);
        mListView = (ListView) fragmentView.findViewById(R.id.MyListView);

        final ProgressDialog dialog = ProgressDialog.show(
                getActivity(), "",
                "Loading Movies list...", true);
        mConcertList = new ArrayList<Concert>();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("app");
        Log.d("concert", "Retrieved " + query + " concerts");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> concertParseList, ParseException e) {

                if (e == null) {
                    Log.d("concert", "Concert size " + concertParseList.size());
                    for(ParseObject parseObject : concertParseList) {
                        Log.d("concert", "TESTING THE TITLE: " + (String) parseObject.get("title"));
                        String title = (String) parseObject.get("title");
                        String link = (String) parseObject.get("link");
                        String imageLink = (String) parseObject.get("imageLink");

                        Concert concert = new Concert(title, link, imageLink);
                        moviesCount ++;
                        mConcertList.add(concert);
                    }
                    mListView.setAdapter(new MyAdapter());
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
                if(moviesCount == concertParseList.size()){
                    dialog.dismiss();
                }
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Concert concertPosition = mConcertList.get(position);
                openBrowser(concertPosition.getLink());
            }
        });

        return fragmentView;
    }

    public void openBrowser(String link){
        // this method is used to redirect to browser
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(browserIntent);
    }

    private class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return mConcertList.size();
        }

        @Override
        public Object getItem(int position) {
            return mConcertList.get(position);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            View rowView = getActivity().getLayoutInflater().inflate(R.layout.row, null);

            Concert rowConcert = mConcertList.get(position);

            TextView textViewRow = (TextView) rowView.findViewById(R.id.textView);
            textViewRow.setText(rowConcert.getTitle());

            ImageView imageView = (ImageView) rowView.findViewById(R.id.imageViewPicasso);
            Picasso.with(getActivity()).load(rowConcert.getImageLink()).into(imageView);

            return rowView;
        }
    }

}
