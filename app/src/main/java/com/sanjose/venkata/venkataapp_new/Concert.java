package com.sanjose.venkata.venkataapp_new;

/**
 * Created by buddhira on 4/5/2015.
 */
public class Concert {

    private String title;
    private String link;
    private String imageLink;

    public Concert(String title, String link, String imageLink) {
        this.title = title;
        this.link = link;
        this.imageLink = imageLink;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getImageLink() {
        return imageLink;
    }
}
