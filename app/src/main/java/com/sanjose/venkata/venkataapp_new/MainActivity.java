package com.sanjose.venkata.venkataapp_new;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;


public class MainActivity extends FragmentActivity {

    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        Log.d("adapter", "adapter size ");

        //addListenerOnButton();
        /*AdView adView = (AdView) findViewById(R.id.adView);
        adView.loadAd(new AdRequest.Builder().build());*/
       /* fragment_mymap = (Button) findViewById(R.id.fragment_mymap);
        fragment_mymap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), Map.class);
                //execute the intent
                startActivity(intent);
            }
        });
        *///Toast.makeText(getApplicationContext(), "Loading Awesomeness, Please wait...", Toast.LENGTH_LONG).show();
    }

    /*public void addListenerOnButton() {
        fragment_mymap = (Button) findViewById(R.id.fragment_mymap);
        fragment_mymap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

            }
        });
    }*/

    // adapter of fragment in general
    public class MyAdapter extends FragmentPagerAdapter{
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d("adapter", "position "+position);
            if(position == 0) {
                Fragment fragmentMap = new MyMapFragment();
                return fragmentMap;
            }else{
                Fragment fragmentMovie = new MovieFragment();
                return fragmentMovie;
            }/* else {
                Fragment fragmentList = new NewsFragment();
                return fragmentList;
            }*/
        }
        // number of fragments
        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0){
                return "PLACES";
            }else if(position ==1){
                return "MOVIES";
            }else if(position == 2){
                return "SPORTS";
            }else{
                return "WEATHER";
            }
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up fragment_mymap, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
