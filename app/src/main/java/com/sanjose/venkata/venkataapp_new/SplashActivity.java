package com.sanjose.venkata.venkataapp_new;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;


public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        TextView textViewTitle = (TextView) findViewById(R.id.textView4);
        TextView textViewSubTitle = (TextView) findViewById(R.id.textView5);

        Typeface typeFaceTitle= Typeface.createFromAsset(getAssets(), "Lobster-Regular.ttf");
        Typeface typeFaceSubtitle= Typeface.createFromAsset(getAssets(), "BreeSerif-Regular.ttf");
        textViewTitle.setTypeface(typeFaceTitle, Typeface.NORMAL);
        textViewSubTitle.setTypeface(typeFaceSubtitle, Typeface.NORMAL);
        //myTextView.setTypeface(Typeface.SERIF, Typeface.BOLD);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nextActivity();
                finish();
            }
        },8000);
    }

    public void nextActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        //execute the intent
        startActivity(intent);
    }
}
