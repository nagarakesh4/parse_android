package com.sanjose.venkata.venkataapp_new;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by buddhira on 4/6/2015.
 */
public class MyMapFragment extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        LinearLayout mLinearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_mymap,
                container, false);
        TextView textViewTitle = (TextView) mLinearLayout.findViewById(R.id.textView6);

            Typeface typeFaceTitle= Typeface.createFromAsset(getActivity().getAssets(), "BreeSerif-Regular.ttf");
        textViewTitle.setTypeface(typeFaceTitle, Typeface.NORMAL);

        ImageButton mButton = (ImageButton) mLinearLayout.findViewById(R.id.ImageButton01);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getActivity(), MapActivity.class);
                //execute the intent
                startActivity(intent);
            }
        });

        // after you've done all your manipulation, return your layout to be shown
        return mLinearLayout;
    }
    /*//MapView mapView;
    Marker marker;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //List<MarkerOptions> markerOptionsAll = new ArrayList<MarkerOptions>();
        *//*View rowView = getActivity().getLayoutInflater().inflate(R.layout.activity_main, null);

        TextView textViewRow2 = (TextView)rowView.findViewById(R.id.textView2);
        TextView textViewRow3 = (TextView)rowView.findViewById(R.id.textView3);
        textViewRow2.setVisibility(TextView.VISIBLE);
        textViewRow3.setVisibility(TextView.VISIBLE);*//*

        GoogleMap googleMap = getMap();
        googleMap.setMyLocationEnabled(true);
        //Log.d("googled", String.valueOf(googleMap.getMyLocation()));
        googleMap.getUiSettings().setZoomControlsEnabled(true);


        // single marker
        // MarkerOptions markerOptions = new MarkerOptions();
        // markerOptions.position(new LatLng(37.340513, -121.898808));

        //multiple markers
        // markerOptionsAll.add(0, markerOptions.position(new LatLng(37.331046, -121.894067)));
        //markerOptionsAll.add(1, markerOptions.position(new LatLng(37.324238, -122.043504)));
        //List<Marker> markers = new ArrayList<Marker>();
        //for custom icon
        //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher));

        // markerOptions.title("Venkata's New House");
        // markerOptions.snippet("This is my new rented home from 3rd April, 2015");
        // googleMap.addMarker(markerOptions);
        // googleMap.addMarker(markerOptionsAll.get(0));

        ParseQuery<ParseObject> query = ParseQuery.getQuery("maps");
        Log.d("concert", "Retrieved " + query + " maps");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> mapParseList, ParseException e) {
                GoogleMap googleMap = getMap();
                int pointCounter = 0;
                List<Marker> markersMap = new ArrayList<Marker>();
                if (e == null) {
                    Log.d("maps", "map size " + mapParseList.size());
                    for(ParseObject parseObject : mapParseList) {

                        Bitmap bmImg = null;
                        try {
                            URL url = new URL((String) parseObject.get("imageUrl"));
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setDoInput(true);
                            conn.connect();
                            InputStream is = conn.getInputStream();
                            bmImg = BitmapFactory.decodeStream(is);
                        }
                        catch (IOException exception)
                        {
                            exception.printStackTrace();
                            bmImg = null;
                        }
                        Marker markerPoints = googleMap.addMarker(new MarkerOptions()
                                        //.position(new LatLng(parseObject.getDouble("latitude"), parseObject.getDouble("longitude")))
                                        .position(new LatLng(parseObject.getParseGeoPoint("location").getLatitude(),
                                                parseObject.getParseGeoPoint("location").getLongitude()))
                                        .title((String) parseObject.get("title"))
                                        .snippet((String) parseObject.get("snippet"))
                                .icon(BitmapDescriptorFactory.fromBitmap(bmImg)));
                                //.icon(BitmapDescriptorFactory.fromBitmap(result))
                        //);
                        if (pointCounter == 0){

                        }
                        markersMap.add(markerPoints);
                        pointCounter++;
                        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                marker.showInfoWindow();
                                //zoom camera
                                onLocationChanged(marker.getPosition().latitude, marker.getPosition().longitude, 15, 80);
                                return true;
                            }
                        });
                    }
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
                onLocationChanged(37.340513, -121.898808, 18, 80);
                //markersMap.get(0).showInfoWindow();
            }
        });

       *//* Marker markerSJ = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.340513, -121.898808))
                .title("Venkata's New House - Legacy Fountain Plaza")
                .snippet("This is my new rented home from 3rd April, 2015")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher)));
        markerSJ.showInfoWindow();

        Marker markerAdobeSJ = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.331046, -121.894067))
                .title("Adobe Head Quarters - San Jose")
                .snippet("This is where I am Interning - Almaden Tower")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_1)));
        //markerAdobeSJ.showInfoWindow();

        Marker markerVPCU = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.324238, -122.043504))
                .title("Valli Pinni's House - Cupertino")
                .snippet("In Patriot Way")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_vpinni)));
        //markerVPCU.showInfoWindow();

        Marker markerRPSG = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(1.431039, 103.844352))
                .title("Radha Pinni's House - Singapore")
                .snippet("In Yishun - 2009")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_rp)));

        Marker markerMVSG = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(1.309430, 103.863190))
                .title("Mavayya's House - Singapore")
                .snippet("City lights, Jellicoe Road")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_mv)));

        Marker markerAKIN = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(28.662143, 77.364749))
                .title("Pedda Akka's House - Ghaziabad")
                .snippet("Vasundhara, Uttar pradesh")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_2)));

        Marker markerSUSB = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(34.430376, -119.886865))
                .title("Sunny's Office - CITRIX")
                .snippet("Goletta, Santa Barbara")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_sun)));

        Marker markerCABL = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(12.981530, 77.677171))
                .title("Chinna Akka's House - Bangalore")
                .snippet("In Khagadasapura - 2011")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_ca)));

        markers.add(markerSJ);
        markers.add(markerAdobeSJ);
        markers.add(markerVPCU);
        markers.add(markerRPSG);
        markers.add(markerMVSG);
        markers.add(markerAKIN);
        markers.add(markerSUSB);
        markers.add(markerCABL);*//*

       // markers.size();
        //googleMap.addMarker(markerOptionsAll);
        //Toast.makeText(getActivity(), "Loading Maps, Please wait...", Toast.LENGTH_LONG).show();
    }*//*

    public void onLocationChanged(double latitude, double longitude, float zoom, float tilt) {
        //LatLng latLng = new LatLng(latitude, longitude);


        //llamarservicio(latitude,longitude,speed);

        // Showing the current location in Google Map

        // googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));


        CameraPosition camPos = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(zoom)
                        //.bearing(location.getBearing())
                .tilt(tilt)
                .build();

        CameraUpdate camUpd3 =
                CameraUpdateFactory.newCameraPosition(camPos);

        GoogleMap googleMap = getMap();
        googleMap.animateCamera(camUpd3);
    }*/
}
