package com.sanjose.venkata.venkataapp_new;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by buddhira on 4/22/2015.
 */
public class MovieFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        LinearLayout mLinearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_movie,
                container, false);
        TextView textViewTitle = (TextView) mLinearLayout.findViewById(R.id.textViewMovies);

        Typeface typeFaceTitle= Typeface.createFromAsset(getActivity().getAssets(), "BreeSerif-Regular.ttf");
        textViewTitle.setTypeface(typeFaceTitle, Typeface.NORMAL);

        ImageButton mButton = (ImageButton) mLinearLayout.findViewById(R.id.ButtonMovie);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getActivity(), BoxOfficeActivity.class);
                //execute the intent
                startActivity(intent);
            }
        });

        // after you've done all your manipulation, return your layout to be shown
        return mLinearLayout;
    }
}
