package com.sanjose.venkata.venkataapp_new;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by buddhira on 4/23/2015.
 */
public class BoxOfficeMovie implements Serializable {

    private static final long serialVersionUID = -8959832007991513854L;

    private String title;
    private int year;
    private String synopsis;
    private String posterUrl;
    private int criticsScore;
    private ArrayList<String> castList;
    private String largePosterUrl;
    //private String criticsConsensus;
    private int audienceScore;

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public int getCriticsScore() {
        return criticsScore;
    }

    public String getCastList() {
        return TextUtils.join(", ", castList);
    }
    public String getLargePosterUrl() {
        return largePosterUrl;
    }

    //public String getCriticsConsensus() {
       // return criticsConsensus;
   // }

    public int getAudienceScore() {
        return audienceScore;
    }
    // Returns a BoxOfficeMovie given the expected JSON
    // BoxOfficeMovie.fromJson(movieJsonDictionary)
    // Stores the `title`, `year`, `synopsis`, `poster` and `criticsScore`
    public static BoxOfficeMovie fromJson(JSONObject jsonObject) {
        BoxOfficeMovie boxOfficeMovie = new BoxOfficeMovie();
        try {
            // Deserialize json into object fields
            boxOfficeMovie.title = jsonObject.getString("title");
            boxOfficeMovie.year = jsonObject.getInt("year");
            boxOfficeMovie.synopsis = jsonObject.getString("synopsis");
            boxOfficeMovie.posterUrl = jsonObject.getJSONObject("posters").getString("thumbnail");
            boxOfficeMovie.criticsScore = jsonObject.getJSONObject("ratings").getInt("critics_score");
            boxOfficeMovie.largePosterUrl = jsonObject.getJSONObject("posters").getString("detailed");
            //boxOfficeMovie.criticsConsensus = jsonObject.getString("critics_consensus");
            boxOfficeMovie.audienceScore = jsonObject.getJSONObject("ratings").getInt("audience_score");
            // Construct simple array of cast names
            boxOfficeMovie.castList = new ArrayList<String>();
            JSONArray abridgedCast = jsonObject.getJSONArray("abridged_cast");
            for (int i = 0; i < abridgedCast.length(); i++) {
                boxOfficeMovie.castList.add(abridgedCast.getJSONObject(i).getString("name"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        // Return new object
        return boxOfficeMovie;
    }

    // Decodes array of box office movie json results into movie model objects
    // BoxOfficeMovie.fromJson(jsonArrayOfMovies)
    public static ArrayList<BoxOfficeMovie> fromJson(JSONArray jsonArray) {
        ArrayList<BoxOfficeMovie> movies = new ArrayList<BoxOfficeMovie>(jsonArray.length());
        // Process each result in json array, decode and convert to movie
        // object
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject moviesJson = null;
            try {
                moviesJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            BoxOfficeMovie movie = BoxOfficeMovie.fromJson(moviesJson);
            if (movie != null) {
                movies.add(movie);
            }
        }

        return movies;
    }
}
