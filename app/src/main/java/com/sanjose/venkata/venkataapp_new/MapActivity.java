    package com.sanjose.venkata.venkataapp_new;

    import android.annotation.TargetApi;
    import android.app.Activity;
    import android.app.ProgressDialog;
    import android.content.Context;
    import android.graphics.Bitmap;
    import android.graphics.BitmapFactory;
    import android.graphics.Color;
    import android.graphics.Typeface;
    import android.os.AsyncTask;
    import android.os.Build;
    import android.os.Bundle;
    import android.os.Handler;
    import android.util.Log;
    import android.view.Gravity;
    import android.view.View;
    import android.view.inputmethod.InputMethodManager;
    import android.widget.Button;
    import android.widget.ImageButton;
    import android.widget.SearchView;
    import android.widget.TextView;
    import android.widget.Toast;

    import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
    import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
    import com.google.android.gms.maps.CameraUpdate;
    import com.google.android.gms.maps.CameraUpdateFactory;
    import com.google.android.gms.maps.GoogleMap;
    import com.google.android.gms.maps.MapFragment;
    import com.google.android.gms.maps.model.BitmapDescriptorFactory;
    import com.google.android.gms.maps.model.CameraPosition;
    import com.google.android.gms.maps.model.LatLng;
    import com.google.android.gms.maps.model.Marker;
    import com.google.android.gms.maps.model.MarkerOptions;
    import com.parse.FindCallback;
    import com.parse.ParseException;
    import com.parse.ParseObject;
    import com.parse.ParseQuery;

    import java.io.IOException;
    import java.io.InputStream;
    import java.net.HttpURLConnection;
    import java.net.URL;
    import java.util.ArrayList;
    import java.util.List;


    public class MapActivity extends Activity {

        List<Marker> markersMap = new ArrayList<>();
        //to initially show at my place
        int pointCounter = 0;
        GoogleMap googleMap;
        Bitmap bmImg = null;
        int currentNextMarker = 0;
        boolean prevFirst = true;

        ProgressDialog progressDialog;
        Context mContext = this;
        private Thread mThread;

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        protected void onCreate(Bundle savedInstanceState) {


            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_map);

            /*SearchView searchViewContext = new SearchView(getApplicationContext());
            int id = searchViewContext.getContext().getResources().getIdentifier("android:id/searchView", null, null);
            TextView textView = (TextView) searchViewContext.findViewById(id);
            textView.setTextColor(Color.BLACK);*/

            final SearchView searchView = (SearchView) findViewById(R.id.searchView);
            //String searchQuery = searchView.getQuery().toString();

            final ImageButton infoButtonPic = (ImageButton) findViewById(R.id.imageButtonInfo);
            infoButtonPic.setVisibility(View.GONE);

            final ImageButton infoButtonDetails = (ImageButton) findViewById(R.id.imageButtonInfoDetails);
            infoButtonDetails.setVisibility(View.GONE);

            Typeface font = Typeface.createFromAsset( getAssets(), "fontawesome-webfont.ttf" );

            final Button buttonNext = (Button) findViewById(R.id.buttonNext);
            buttonNext.setTypeface(font);
            buttonNext.setVisibility(View.GONE);
            final Button buttonPrev = (Button) findViewById(R.id.buttonPrevious);
            buttonPrev.setTypeface(font);
            buttonPrev.setVisibility(View.GONE);

            /*TextView textViewSubTitle = (TextView) findViewById(R.id.textView7);

            Typeface typeFaceTitle= Typeface.createFromAsset(getAssets(), "Lobster-Regular.ttf");
            Typeface typeFaceSubtitle= Typeface.createFromAsset(getAssets(), "BreeSerif-Regular.ttf");
            textViewSubTitle.setTypeface(typeFaceTitle, Typeface.NORMAL);*/

            /*menu.findItem(R.id.share).setIcon(
                    new IconDrawable(this, IconValue.fa_share)
                            .colorRes(R.color.ab_icon)
                            .actionBarSize());*/






            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);


            mContext = this;
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setCancelable(false);
            //progressDialog.setMessage("Loading....");
            //progressDialog.show();

            ParseQuery<ParseObject> query = ParseQuery.getQuery("maps");
            Log.d("concert", "Retrieved " + query + " maps");

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> mapParseList, ParseException e) {
                    if (e == null) {
                        Log.d("maps", "map size " + mapParseList.size());
                        for (ParseObject parseObject : mapParseList) {
                            try {
                                URL url = new URL((String) parseObject.get("imageUrl"));
                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                conn.setDoInput(true);
                                conn.connect();
                                InputStream is = conn.getInputStream();
                                bmImg = BitmapFactory.decodeStream(is);
                            } catch (IOException exception) {
                                exception.printStackTrace();
                                bmImg = null;
                            }
                            Marker markerPoints = googleMap.addMarker(new MarkerOptions()
                                    //.position(new LatLng(parseObject.getDouble("latitude"), parseObject.getDouble("longitude")))
                                    .position(new LatLng(parseObject.getParseGeoPoint("location").getLatitude(),
                                            parseObject.getParseGeoPoint("location").getLongitude()))
                                    .title((String) parseObject.get("title"))
                                    .snippet((String) parseObject.get("snippet"))
                                    .icon(BitmapDescriptorFactory.fromBitmap(bmImg)));
                            //.icon(BitmapDescriptorFactory.fromBitmap(result))
                            //);
                            if (pointCounter == 0) {
                                markerPoints.showInfoWindow();
                            }
                            markersMap.add(markerPoints);

                            //bitmaps.add(0, BitmapDescriptorFactory.fromBitmap(bmImg));
                            pointCounter++;
                            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(Marker marker) {
                                    infoButtonPic.setVisibility(View.GONE);
                                    infoButtonDetails.setVisibility(View.VISIBLE);
                                    marker.showInfoWindow();
                                    currentNextMarker = markersMap.indexOf(marker);
                                   // retrieveNextMarkerInfo(markersMap.indexOf(marker));
                                    //zoom camera
                                    onLocationChanged(marker.getPosition().latitude, marker.getPosition().longitude, 18, 80, true);
                                    //return true to hide directions icon on map when clicked on a marker
                                    return true; // will default the directions icon visibility to true
                                }
                            });
                            if((mapParseList.size()-1)==pointCounter) {
                                buttonNext.setVisibility(View.VISIBLE);
                                buttonPrev.setVisibility(View.VISIBLE);
                                infoButtonPic.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                    onLocationChanged(37.340513, -121.898808, 13, 80, true);
                    //markersMap.get(0).showInfoWindow();

                }
            });


            infoButtonPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    showInformationDialog(true);
                }
            });

            infoButtonDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    showInformationDialog(false);
                }
            });

            buttonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    infoButtonPic.setVisibility(View.VISIBLE);
                    infoButtonDetails.setVisibility(View.GONE);
                    /*if (selectedMarker == currentNextMarker) {
                        selectedMarker++;
                    }*/
                    prevFirst = false;
                    currentNextMarker++;
                    if (currentNextMarker == markersMap.size())
                        currentNextMarker = 0;
                    markersMap.get(currentNextMarker).showInfoWindow();
                    onLocationChanged(markersMap.get(currentNextMarker).getPosition().latitude,
                            markersMap.get(currentNextMarker).getPosition().longitude, 13, 80, true);
                }
            });

            buttonPrev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    infoButtonPic.setVisibility(View.VISIBLE);
                    infoButtonDetails.setVisibility(View.GONE);
                    /*if(selectedMarker == currentNextMarker)
                        selectedMarker--;*/
                    if(currentNextMarker <= 0 || prevFirst) {
                        currentNextMarker = (markersMap.size()-1);
                        prevFirst = false;
                    }else{
                        currentNextMarker--;
                    }
                    markersMap.get(currentNextMarker).showInfoWindow();
                    onLocationChanged(markersMap.get(currentNextMarker).getPosition().latitude,
                            markersMap.get(currentNextMarker).getPosition().longitude, 13, 80, true);
                  }
            });


            searchView.setQueryHint(this.getString(R.string.search));
            //searchView.setBackgroundColor(Color.WHITE);
            searchView.setOnQueryTextListener
                (new SearchView.OnQueryTextListener() {

                     @Override
                     public boolean onQueryTextSubmit(String s) {
                         infoButtonPic.setVisibility(View.GONE);
                         infoButtonDetails.setVisibility(View.VISIBLE);
                         progressDialog.show();
                         progressDialog.setMessage("Please wait while I search and take you there...");

                         //Toast.makeText(getApplicationContext(), "Please wait while I search and take you there!", Toast.LENGTH_SHORT).show();
                         ParseQuery<ParseObject> query = ParseQuery.getQuery("maps");
                         query.whereContains("Name", s);
                         //query.whereContains("title", s); this will serve as AND query
                         query.findInBackground(new FindCallback<ParseObject>() {
                         @Override
                             public void done(List<ParseObject> parseObjects, ParseException e) {
                             Log.d("Retrieved result", String.valueOf(parseObjects.size()));
                             if (e == null && parseObjects.size() != 0 && parseObjects.size()< 2) {

                                 for (final ParseObject parseObject : parseObjects) {
                                     new Handler().postDelayed(new Runnable() {
                                         @Override
                                         public void run() {
                                        progressDialog.dismiss();
                                        onLocationChanged(parseObject.getParseGeoPoint("location").getLatitude(),
                                             parseObject.getParseGeoPoint("location").getLongitude()
                                             , 18, 80, false);
                                         }
                                     }, 2000);
                                 }
                             } else if (parseObjects.size() > 1) {
                                 new Handler().postDelayed(new Runnable() {
                                     @Override
                                     public void run() {
                                         progressDialog.dismiss();
                                         SearchView searchView = (SearchView) findViewById(R.id.searchView);
                                         searchView.clearFocus();
                                         Toast.makeText(getApplicationContext(), "Multiple results exists, Try refining your search", Toast.LENGTH_LONG).show();
                                     }
                                 }, 1000);
                                 }else {
                                 new Handler().postDelayed(new Runnable() {
                                     @Override
                                     public void run() {
                                         progressDialog.dismiss();
                                         SearchView searchView = (SearchView) findViewById(R.id.searchView);
                                         searchView.clearFocus();
                                         Toast.makeText(getApplicationContext(), "Oh Oh!, No Such person or place exists in my data, please try other keywords", Toast.LENGTH_LONG).show();
                                     }
                                 }, 1000);
                                }
                             }
                         });
                         return false;
                     }

                     @Override
                     public boolean onQueryTextChange(String s) {
                         return true;
                     }
                 });

            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.loadingAwesome), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();

        }

        private void showInformationDialog(boolean status) {
            String textToShow;
            Effectstype effectstype;
            if(status){
                textToShow = "Tap or Touch on picture to zoom in";
                effectstype = Effectstype.Flipv;
            }else {
                textToShow = "Tap or Touch on picture to see details";
                effectstype = Effectstype.Slit;
            }

            final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(this);
            dialogBuilder
                    .withTitle("Tip")
                    .withMessage(textToShow)
                    .withEffect(effectstype)
                    .withDialogColor("#4d4d4d")
                    .withIcon(R.drawable.ic_launcher_qm)
                    .withButton1Text("Ok")
                    //.setCustomView(fragment_mymap, view.getContext())
                    .setButton1Click(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogBuilder.dismiss();
                        }
                    })
                    .show();
        }


        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public void onLocationChanged(double latitude, double longitude, float zoom, float tilt, final boolean map) {
            SearchView searchView = (SearchView) findViewById(R.id.searchView);
            /*InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);*/
            searchView.clearFocus();

            GoogleMap googleMap;
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            CameraPosition camPos = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude))
                    .zoom(zoom)
                    .bearing(-90)
                    .tilt(tilt)
                    .build();

            CameraUpdate camUpd3 =
                    CameraUpdateFactory.newCameraPosition(camPos);
            googleMap.animateCamera(camUpd3,
                    1500,
                    new GoogleMap.CancelableCallback() {

                        @Override
                        public void onFinish() {
                            if(!map){
                                Toast.makeText(getApplicationContext(), "Tap or Touch on picture to see details", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancel() {
                        }
                    }
            );
            /*if(map==false) {
                //googleMap.animateCamera(CameraUpdateFactory.scrollBy(250-(float)Math.random()*500-250, 250-(float)Math.random()*500), 18,null);

            }else{
                //googleMap.moveCamera(camUpd3);
            }*/
        }
    }
